# Oggy - Backend

This repository contains the [Scala](https://www.scala-lang.org/) source code for the chat application of the [2022 FRP Project](https://docs.google.com/document/d/1anV0G7cVZiEljeEf_qi-5mF_ErKitjxJkAWrq0JjRXY/edit#heading=h.fylfh8oec611).

## Requirements

- [Java 11](https://www.oracle.com/java/technologies/downloads/#java11)
- [SBT](https://www.scala-sbt.org/download.html)

## Local installation

```sh
# Clone the project
git clone git@gitlab.polytech.umontpellier.fr:akk-chat/backend.git
cd backend
# Start the server
sbt run
```

### Format source code

```sh
sbt scalafmt
```

## Authors

- [Julian Labatut](mailto:julian.labatut@etu.umontpellier.fr)
- [Kalil Pelissier](mailto:kalil.pelissier@etu.umontpellier.fr)
- [Malo Polese](mailto:malo.polese@etu.umontpellier.fr)
