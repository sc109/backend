lazy val akkaHttpVersion = "10.2.9"
lazy val akkaVersion    = "2.6.19"

// Run in a separate JVM, to make sure sbt waits until all threads have
// finished before returning.
// If you want to keep the application running while executing other
// sbt tasks, consider https://github.com/spray/sbt-revolver/
fork := true
// scalafmtOnCompile := true

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization    := "fr.do.polytech",
      scalaVersion    := "2.13.4"
    )),
    name := "backend-aka",
    libraryDependencies ++= Seq(
      "ch.megard" %% "akka-http-cors" % "1.1.3",
      "com.typesafe.akka" %% "akka-http"                % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json"     % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-actor-typed"         % akkaVersion,
      "com.typesafe.akka" %% "akka-stream"              % akkaVersion,
      "com.typesafe.akka" %% "akka-stream-typed"        % "2.6.19",
      "com.github.scredis" %% "scredis"                 % "2.4.3",
      "com.typesafe.akka" %% "akka-stream-testkit"      % akkaVersion % Test,
      "ch.qos.logback"    % "logback-classic"           % "1.2.11",

      "com.typesafe.akka" %% "akka-http-testkit"        % akkaHttpVersion % Test,
      "com.typesafe.akka" %% "akka-actor-testkit-typed" % akkaVersion     % Test,
      "org.scalatest"     %% "scalatest"                % "3.2.11"         % Test,
      "org.mongodb.scala" %% "mongo-scala-driver"       % "4.5.1"
    )
  )
