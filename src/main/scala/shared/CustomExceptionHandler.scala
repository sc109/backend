package shared

import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.StatusCodes.BadRequest
import akka.http.scaladsl.server.Directives.{complete, extractUri}
import akka.http.scaladsl.server.ExceptionHandler

class BadRequestException extends Exception {}

object CustomExceptionHandler {
  val exceptionHandler: ExceptionHandler =
    ExceptionHandler { case _: BadRequestException =>
      extractUri { uri =>
        complete(HttpResponse(BadRequest, entity = "Bad request"))
      }
    }
}
