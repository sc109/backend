package api

import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.http.scaladsl.server.PathMatchers.Segment
import akka.http.scaladsl.server.Directives._
import chat.entitys.{OggyMessage, Room, UserRoom}
import org.mongodb.scala.bson.collection.mutable.Document
import spray.json.DefaultJsonProtocol

import scala.concurrent.Future
import scala.util.{Failure, Success}

case class HttpController() {

  def getChannels(chatRoomName: String) {
    MongoDB.Message.findAllByRoom(chatRoomName)
  }

  import DefaultJsonProtocol._
  import chat.marshaller.MessageMarshaller.messageJsonFormat
  import chat.marshaller.RoomMarshaller._
  import spray.json._
  import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._

  val route = {
    concat(
      path("api" / "message" / Segment) { room =>
        get {
          val future: Future[Seq[Document]] =
            MongoDB.Message.findAllByRoom(room)
          onComplete(future) {
            case Success(list) =>
              complete(
                HttpEntity(
                  ContentTypes.`application/json`,
                  list.toList
                    .map(_.toJson.parseJson.convertTo[OggyMessage])
                    .toJson
                    .prettyPrint
                )
              )
            case Failure(e) =>
              complete(StatusCodes.NotFound)
          }
        }
      },
      path("api" / "room" / Segment) { room =>
        concat(
          get {
            val future: Future[Option[Document]] = MongoDB.ChatRoom.get(room)
            onComplete(future) {
              case Success(Some(room)) =>
                complete(
                  HttpEntity(
                    ContentTypes.`application/json`,
                    room.toJson.parseJson.convertTo[Room].toJson.prettyPrint
                  )
                )
              case Success(None) => complete(StatusCodes.NotFound)
              case Failure(e) =>
                complete(StatusCodes.NotFound)
            }
          }
        )
      },
      path("api" / "user" / Segment / "room") { pseudo =>
        concat(
          post {
            entity(as[Room]) { room =>
              MongoDB.UserChatRoom.createIfNotExist(pseudo, room.slug)
              MongoDB.ChatRoom.createIfNotExist(room.slug)
              complete(
                StatusCodes.Created,
                HttpEntity(ContentTypes.`application/json`, "")
              )
            }
          },
          get {
            val future: Future[Seq[Document]] =
              MongoDB.UserChatRoom.findUserRoom(pseudo)
            onComplete(future) {
              case Success(list) =>
                complete(
                  HttpEntity(
                    ContentTypes.`application/json`,
                    list.toList
                      .map(_.toJson.parseJson.convertTo[UserRoom])
                      .map(ur => Room(ur.channel))
                      .toJson
                      .prettyPrint
                  )
                )
              case Failure(e) =>
                complete(
                  StatusCodes.NotFound,
                  HttpEntity(
                    ContentTypes.`application/json`,
                    ""
                  )
                )
            }
          }
        )
      }
    )
  }
}
