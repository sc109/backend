package api

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorSystem, Behavior, PostStop}
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives.concat
import akka.http.scaladsl.Http.ServerBinding
import akka.http.scaladsl.server.Directives.handleExceptions
import akka.util.ByteString
import ch.megard.akka.http.cors.scaladsl.CorsDirectives.cors
import ch.megard.akka.http.cors.scaladsl.settings.CorsSettings
import chat.actors.ChatDispatcherActor
import shared.CustomExceptionHandler

import java.util.UUID
import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.Future
import scala.util.{Failure, Success}

object HttpServer {

  sealed trait Status
  private final case class StartFailed(cause: Throwable) extends Status
  private final case class Started(binding: ServerBinding) extends Status
  case object Stop extends Status

  def apply(host: String, port: Int): Behavior[Status] = Behaviors.setup {
    ctx =>
      implicit val system: ActorSystem[Nothing] = ctx.system

      val serverId = UUID.randomUUID()

      val chatDispatcherActor =
        ctx.spawn(ChatDispatcherActor(serverId), "ChatDispatcherActor")
      val pingCounter = new AtomicInteger()

      val serverBinding: Future[Http.ServerBinding] =
        Http()
          .newServerAt(host, port)
          .adaptSettings(
            _.mapWebsocketSettings(
              _.withPeriodicKeepAliveData(() =>
                ByteString(s"debug-${pingCounter.incrementAndGet()}")
              )
            )
          )
          .bind(
            handleExceptions(CustomExceptionHandler.exceptionHandler) {
              cors(CorsSettings.defaultSettings) {
                concat(
                  WebsocketController(chatDispatcherActor)(system).routes,
                  HttpController().route
                )
              }
            }
          )

      ctx.pipeToSelf(serverBinding) {
        case Success(binding) => Started(binding)
        case Failure(ex)      => StartFailed(ex)
      }

      def running(binding: ServerBinding): Behavior[Status] =
        Behaviors
          .receiveMessagePartial[Status] { case Stop =>
            ctx.log.info(
              "Stopping server http://{}:{}/",
              binding.localAddress.getHostString,
              binding.localAddress.getPort
            )
            Behaviors.stopped
          }
          .receiveSignal { case (_, PostStop) =>
            binding.unbind()
            Behaviors.same
          }

      def starting(wasStoped: Boolean): Behaviors.Receive[Status] =
        Behaviors.receiveMessage[Status] {
          case StartFailed(cause) =>
            throw new RuntimeException("Server faild to start", cause)
          case Started(binding) =>
            system.log.info(
              "Server with identifier {} online at http://{}:{}/",
              serverId,
              binding.localAddress.getHostString,
              binding.localAddress.getPort
            )
            if (wasStoped) ctx.self ! Stop
            running(binding)
          case Stop =>
            starting(wasStoped = true)
        }
      starting(wasStoped = false)
  }
}
