package api

import akka.NotUsed
import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.scaladsl.model.ws.{Message, TextMessage}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives.{
  complete,
  extractWebSocketUpgrade,
  get,
  path
}
import akka.stream.scaladsl.{Flow, Sink, Source}
import akka.stream.OverflowStrategy
import chat.actors.ChatDispatcherActor
import akka.stream.typed.scaladsl.{ActorSink, ActorSource}
import chat.entitys.OggyMessage
import spray.json._
import akka.http.scaladsl.server.PathMatchers.Segment
import akka.http.scaladsl.server.Directives._

case class WebsocketController(
    chatDispatcherActor: ActorRef[ChatDispatcherActor.Command]
)(implicit system: ActorSystem[Nothing]) {
  import chat.marshaller.MessageMarshaller.messageJsonFormat

  def in(pseudo: String): Sink[Message, NotUsed] = Flow[Message]
    .map { case TextMessage.Strict(msg) =>
      ChatDispatcherActor.Command.IncomingMessage(
        msg.parseJson.convertTo[OggyMessage]
      )
    }
    .to(
      ActorSink.actorRef[ChatDispatcherActor.Command](
        chatDispatcherActor,
        ChatDispatcherActor.Command.ConnectionClosed(pseudo),
        { case ex => ChatDispatcherActor.Command.ConnectionFailure(ex) }
      )
    )

  def out(pseudo: String): Source[Message, Unit] = ActorSource
    .actorRef[ChatDispatcherActor.Outgoing](
      { case ChatDispatcherActor.Outgoing.Completed => },
      { case ChatDispatcherActor.Outgoing.Failure(ex) => ex },
      10,
      OverflowStrategy.dropHead
    )
    .mapMaterializedValue { client =>
      chatDispatcherActor ! ChatDispatcherActor.Command.Connected(
        client,
        pseudo
      )
    }
    .map {
      case ChatDispatcherActor.Outgoing.OutgoingMessage(msg) =>
        TextMessage.Strict(msg.toJson.prettyPrint)
      case _ => TextMessage.Strict("")
    }

  val routes: Route = get {
    path("oggy" / Segment) { pseudo =>
      extractWebSocketUpgrade { upgrade =>
        complete(upgrade.handleMessagesWithSinkSource(in(pseudo), out(pseudo)))
      }
    }
  }
}
