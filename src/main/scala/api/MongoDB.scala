package api

import chat.entitys.{OggyMessage, User}
import com.mongodb.{ServerApi, ServerApiVersion}
import com.typesafe.config.{Config, ConfigFactory}
import org.mongodb.scala.bson.BsonDocument
import org.mongodb.scala.bson.collection.mutable.Document
import org.mongodb.scala.bson.conversions.Bson
import org.mongodb.scala.{
  ConnectionString,
  MongoClient,
  MongoClientSettings,
  MongoCollection,
  MongoDatabase
}
import org.mongodb.scala.model.Filters.{equal, and}

import scala.concurrent.Future
import scala.util.Success

object MongoDB {

  private val rootConfig: Config = ConfigFactory.load()

  private val uri = rootConfig.getString("mongo.uri")

  private val mongoClientSettings = MongoClientSettings
    .builder()
    .applyConnectionString(ConnectionString(uri))
    .serverApi(ServerApi.builder().version(ServerApiVersion.V1).build())
    .build()

  private val mongoClient = MongoClient(mongoClientSettings)

  val database: MongoDatabase = mongoClient.getDatabase("oggy")

  private def insertIfNotExist(
      collection: MongoCollection[Document],
      doc: Document,
      query: Bson
  ) = {
    collection
      .countDocuments(query)
      .andThen({ case Success(value: Long) =>
        if (value == 0) collection.insertOne(doc).toFuture()
      })
      .subscribe((_) => println("createIfNotExist success"))
  }

  object User {
    import spray.json._
    import chat.marshaller.UserMarshaller.userJsonFormat

    private val collection: MongoCollection[Document] =
      database.getCollection("user")

    def createIfNotExist(user: User) {
      val doc: Document = Document(user.toJson.prettyPrint)
      insertIfNotExist(collection, doc, equal("pseudo", user.pseudo))
    }
  }

  object UserChatRoom {

    private val collection: MongoCollection[Document] =
      database.getCollection("user-chatroom")

    def createIfNotExist(pseudo: String, chatRoomName: String) {
      val doc: Document =
        Document("pseudo" -> pseudo, "channel" -> chatRoomName)
      insertIfNotExist(
        collection,
        doc,
        and(equal("pseudo", pseudo), equal("channel", chatRoomName))
      )
    }

    def findUserRoom(pseudo: String): Future[Seq[Document]] = {
      collection.find(equal("pseudo", pseudo)).toFuture()
    }
  }

  object ChatRoom {
    private val collection: MongoCollection[Document] =
      database.getCollection("chat-room")

    def createIfNotExist(chatRoomName: String) {
      val doc: Document = Document("slug" -> chatRoomName)
      insertIfNotExist(collection, doc, equal("slug", chatRoomName))
    }

    def get(chatRoomName: String): Future[Option[Document]] = {
      collection.find(equal("slug", chatRoomName)).headOption()
    }
  }

  object Message {
    private val collection: MongoCollection[Document] =
      database.getCollection("message")

    def createMessage(chatRoomName: String, message: OggyMessage) {
      val doc: Document = Document(
        "kind" -> message.kind,
        "channel" -> chatRoomName,
        "text" -> message.text,
        "timestamp" -> message.timestamp,
        "author" -> Document(
          "pseudo" -> message.author.pseudo
        )
      )

      collection
        .insertOne(doc)
        .subscribe(
          (_) => println("Add Message Success"),
          (e) => println("Error", e)
        )
    }

    def findAllByRoom(chatRoomName: String): Future[Seq[Document]] = {
      collection.find(equal("channel", chatRoomName)).toFuture()
    }
  }
}
