package chat.actors

import akka.actor.ActorSystem
import akka.actor.typed.{ActorRef, Behavior, Terminated}
import akka.actor.typed.scaladsl.Behaviors
import chat.entitys.OggyMessage
import scredis.{PubSubMessage, Redis, RedisConfig, SubscriberClient}
import api.MongoDB
import chat.entitys.{OggyMessage, User}
import chat.serialization.MessageSerializer._

import java.util.UUID
import scala.concurrent.Future
import scala.util.{Failure, Success}

object ChatRoomActor {
  trait Chat
  case class RequestTrackUser(
      roomId: String,
      userId: String,
      client: ActorRef[ChatDispatcherActor.Outgoing]
  ) extends Chat

  def apply(serverId: UUID, roomId: String, redis: Redis): Behavior[Chat] = {
    Behaviors.setup { context =>
      implicit val system: ActorSystem = ActorSystem("actor-system")

      val subscriptionHandler: Function[PubSubMessage, Unit] = {
        case m: PubSubMessage.Subscribe =>
          println(s"Subscribed to channel ${m.channel}")

        case m: PubSubMessage.PMessage =>
          println(
            s"Received message for pattern ${m.pattern} on channel ${m.channel} with data ${m
                .readAs[String]()}"
          )

          // Regex to check if server publisher is not actual server
          val otherOrigin = (s"^(?!${serverId.toString}).*:channel:$roomId").r

          otherOrigin.findFirstMatchIn(m.channel) match {
            case Some(_) =>
              context.self ! UserActor.ChatMessage(m.readAs[OggyMessage]())
          }

        case e: PubSubMessage.Error => println(s"Scredis received error $e")
      }

      val subscriberClient =
        SubscriberClient(subscription = subscriptionHandler)

      val subscribeF: Future[Int] =
        subscriberClient.pSubscribe(s"*:channel:$roomId")

      register(Map.empty, redis, serverId, roomId)
    }
  }

  def register(
      userInRoom: Map[String, ActorRef[UserActor.Chat]],
      redis: Redis,
      serverId: UUID,
      roomId: String
  ): Behavior[Chat] = {
    Behaviors
      .receive[Chat] { (context, message) =>
        message match {
          case RequestTrackUser(_, pseudo, client) =>
            userInRoom.get(pseudo) match {
              case Some(_) => Behaviors.same
              case None =>
                context.log.info("Creating user actor for {}", pseudo)
                val userActor: ActorRef[UserActor.Chat] =
                  context.spawn(UserActor(client), pseudo)

                // Create user in database
                MongoDB.User.createIfNotExist(User(pseudo))

                register(
                  userInRoom + (pseudo -> userActor),
                  redis,
                  serverId,
                  roomId
                )
            }

          case ChatDispatcherActor.Command.ConnectionClosed(
                pseudo: String
              ) =>
            userInRoom.get(pseudo) match {
              case Some(userRef) =>
                userRef ! ChatDispatcherActor.Command.ConnectionClosed(pseudo)
              case None => println("L'utilisateur n'exite pas dans cette room")
            }
            register(userInRoom - pseudo, redis, serverId, roomId)

          case message @ UserActor.ChatMessage(msg) =>
            context.log.info("Publish message to redis channel")
            redis.publish(s"${serverId.toString}:channel:$roomId", msg)
            /*
            .onComplete {
              case Success(count) =>
                context.log.info(s"Message published to $count clients")
              case Failure(e) =>
                context.log.error(
                  s"An error occured while trying to send a message: $e"
                )
            }
             */

            MongoDB.Message.createMessage(roomId, msg)

            userInRoom.foreach { case (_, ref) =>
              ref ! message
            }
            Behaviors.same
        }
      }
  }

}
