package chat.actors

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior, SupervisorStrategy}
import spray.json.JsonParser.ParsingException
import chat.entitys.{OggyMessage, User}
import scredis.Redis
import api.MongoDB

import java.util.UUID
import spray.json.JsonParser.ParsingException

import scala.collection.mutable
import scala.collection.mutable.{Map, Set}
import scala.util.Success

object ChatDispatcherActor {
  sealed trait Command
  object Command {
    case class IncomingMessage(msg: OggyMessage) extends Command
    case class Connected(actorRef: ActorRef[Outgoing], userId: String)
        extends Command
    case class ConnectionFailure(ex: Throwable) extends Command
    case class ConnectionClosed(userId: String)
        extends Command
        with UserActor.Chat
        with ChatRoomActor.Chat
  }

  sealed trait Outgoing
  object Outgoing {
    case class OutgoingMessage(msg: OggyMessage) extends Outgoing
    case object Completed extends Outgoing
    case class Failure(ex: Exception) extends Outgoing
  }

  def apply(serverId: UUID): Behavior[Command] = {
    Behaviors.setup { context =>
      connected(serverId, Map(), Map())
    }
  }

  def connected(
      serverId: UUID,
      rooms: Map[String, ActorRef[ChatRoomActor.Chat]],
      connectedUsers: Map[String, ActorRef[Outgoing]]
  ): Behavior[Command] = {

    def trackUserInRoom(
        oggyMessage: OggyMessage,
        roomRef: ActorRef[ChatRoomActor.Chat]
    ) = {
      roomRef ! ChatRoomActor.RequestTrackUser(
        oggyMessage.channel,
        oggyMessage.author.pseudo,
        connectedUsers.get(oggyMessage.author.pseudo) match {
          case Some(userRef) => userRef
        }
      )
    }

    Behaviors.receive { (context, message) =>
      message match {
        case Command.Connected(outgoing: ActorRef[Outgoing], pseudo: String) =>
          context.log.info("new connexion receive ", pseudo)
          connectedUsers.put(pseudo, outgoing)
          connected(serverId, rooms, connectedUsers)

        case Command.IncomingMessage(oggyMessage: OggyMessage) =>
          rooms.get(oggyMessage.channel) match {
            case Some(roomRef) =>
              println("\n\nOK la room existe !!")
              trackUserInRoom(oggyMessage, roomRef)
              roomRef ! UserActor.ChatMessage(oggyMessage)
            case None =>
              val redis = Redis()
              val room = context.spawn(
                ChatRoomActor(serverId, oggyMessage.channel, redis),
                oggyMessage.channel
              )
              trackUserInRoom(oggyMessage, room)
              rooms.put(oggyMessage.channel, room)
              room ! UserActor.ChatMessage(oggyMessage)
          }

          // Create chatroom in mongodb
          MongoDB.UserChatRoom.createIfNotExist(
            oggyMessage.author.pseudo,
            oggyMessage.channel
          )

          connected(serverId, rooms, connectedUsers)

        case Command.ConnectionClosed(pseudo) =>
          context.log.info("Connection close")

          rooms.foreach { case (_, ref) =>
            ref ! Command.ConnectionClosed(pseudo)
          }

          connectedUsers.remove(pseudo) match {
            case Some(_) => println("User removed")
            case None    => println("Pas d'utilisateur")
          }
          connected(serverId, rooms, connectedUsers)

        case Command.ConnectionFailure(ex) =>
          ex match {
            case e: ParsingException =>
              context.log.info("400 - bad request ParsingException")
            case e => context.log.info("500 - unknown error")
          }
          Behaviors
            .supervise(connected(serverId, rooms, connectedUsers))
            .onFailure(
              SupervisorStrategy.restart.asInstanceOf[SupervisorStrategy]
            )
        case _ =>
          Behaviors.same
      }
    }
  }
}
