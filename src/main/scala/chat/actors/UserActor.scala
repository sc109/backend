package chat.actors

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import api.MongoDB
import chat.entitys.OggyMessage

object UserActor {
  trait Chat
  case class ChatMessage(oggMsg: OggyMessage)
      extends Chat
      with ChatRoomActor.Chat

  import ChatDispatcherActor.Outgoing

  def apply(client: ActorRef[Outgoing]): Behavior[Chat] =
    Behaviors.receive { (context, message) =>
      message match {
        case ChatMessage(oggyMsg) =>
          context.log.info(
            "from: " + oggyMsg.author.pseudo + " : " + oggyMsg.text
          )
          client ! Outgoing.OutgoingMessage(oggyMsg)
          Behaviors.same

        case ChatDispatcherActor.Command.ConnectionClosed(userId) =>
          context.log.info("stop user actor: ", userId)
          Behaviors.stopped
      }
    }
}
