package chat.entitys

case class Room(
    slug: String
) {}

case class UserRoom(
    pseudo: String,
    channel: String
) {}
