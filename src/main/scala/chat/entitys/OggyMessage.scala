package chat.entitys

case class OggyMessage(
    kind: String,
    channel: String,
    text: String,
    timestamp: Long,
    author: User
) {}
