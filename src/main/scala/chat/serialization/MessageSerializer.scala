package chat.serialization

import chat.entitys.OggyMessage
import scredis.serialization._
import spray.json._

object MessageSerializer {
  import chat.marshaller.MessageMarshaller.messageJsonFormat

  implicit object MessageWritter extends Writer[OggyMessage] {
    override protected def writeImpl(msg: OggyMessage): Array[Byte] = {
      msg.toJson.prettyPrint.getBytes(("UTF-8"))
    }
  }

  implicit object MessageReader extends Reader[OggyMessage] {
    override protected def readImpl(bytes: Array[Byte]): OggyMessage = {
      val msgString = bytes.map(_.toChar).mkString
      msgString.parseJson.convertTo[OggyMessage]
    }
  }
}
