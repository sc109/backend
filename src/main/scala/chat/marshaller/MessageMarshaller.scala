package chat.marshaller

import spray.json.DefaultJsonProtocol

import chat.entitys.OggyMessage

object MessageMarshaller {
  import DefaultJsonProtocol._
  import UserMarshaller.userJsonFormat

  implicit val messageJsonFormat = jsonFormat5(OggyMessage)
}
