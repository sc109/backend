package chat.marshaller

import spray.json.DefaultJsonProtocol
import chat.entitys.User

object UserMarshaller {
  import DefaultJsonProtocol._

  implicit val userJsonFormat = jsonFormat1(User)
}
