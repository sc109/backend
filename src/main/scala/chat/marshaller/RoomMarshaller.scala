package chat.marshaller

import chat.entitys.{Room, UserRoom}
import spray.json.DefaultJsonProtocol

object RoomMarshaller {
  import DefaultJsonProtocol._
  import UserMarshaller.userJsonFormat

  implicit val roomJsonFormat = jsonFormat1(Room)
  implicit val userRoomJsonFormat = jsonFormat2(UserRoom)
}
