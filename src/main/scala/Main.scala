import akka.actor.typed.ActorSystem
import api.HttpServer

object Main {
  def main(args: Array[String]): Unit = {
    ActorSystem(HttpServer("localhost", 8080), "chat-system")
  }
}
