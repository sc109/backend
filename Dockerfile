FROM hseeberger/scala-sbt:17.0.2_1.6.2_3.1.1

WORKDIR /backend

COPY src ./src
COPY project ./project
COPY .scalafmt.conf ./scalafmt.conf
COPY build.sbt ./build.sbt

RUN sbt compile

EXPOSE 8080

ENTRYPOINT [ "sbt", "run"]